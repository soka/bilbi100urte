[TOC]

# Puente de San Antón

![](img/01-puente-sananton/diapo_01_sananton-pagina001.png)

![](img/01-puente-sananton/diapo_02_sananton-pagina001.png)

![](img/01-puente-sananton/diapo_03_sananton-pagina001.png)

![](img/01-puente-sananton/diapo_04_sananton-pagina001.png)

![](img/01-puente-sananton/diapo_05_sananton-pagina001.png)

![](img/01-puente-sananton/diapo_06_sananton-pagina001.png)

# Urazurrutia 

![](img/02-urazurrutia/diapo_07_urazurrutia-pagina001.png)

![](img/02-urazurrutia/diapo_08_urazurrutia-pagina001.png)

![](img/02-urazurrutia/diapo_09_urazurrutia-pagina001.png)

![](img/02-urazurrutia/diapo_10_urazurrutia-pagina001.png)

# Casa Socorro

![](img/07-casa-socorro/diapo_21-casa-socorro-pagina001.png)

![](img/07-casa-socorro/diapo_22-casa-socorro-pagina001.png)

![](img/07-casa-socorro/diapo_23-casa-socorro-pagina001.png)

![](img/07-casa-socorro/diapo_24-casa-socorro-pagina001.png)

![](img/07-casa-socorro/diapo_25-casa-socorro-pagina001.png)

![](img/07-casa-socorro/diapo_26-casa-socorro-pagina001.png)


# Iturburu

![](img/03-iturburu/diapo_11_iturburu-pagina001.png)

![](img/03-iturburu/diapo_12_iturburu-pagina001.png)


# Cantarranas

![](img/05-cantarranas/diapo_14-cantarranas-pagina001.png)

![](img/05-cantarranas/diapo_15-cantarranas-pagina001.png)

![](img/05-cantarranas/diapo_16-cantarranas-pagina001.png)

![](img/05-cantarranas/diapo_17-cantarranas-pagina001.png)

# Miribilla

![](img/06-miribilla/diapo_18-minas-pagina001.png)

![](img/06-miribilla/diapo_19-minas-pagina001.png)

![](img/06-miribilla/diapo_20-minas-pagina001.png)

# Casa de goma 

_"La Casa de Goma que se la llamaba así por qué nunca dejaba de acoger a temporeros y trabajadores que llegaban al barrio procedentes de todos los lugares para trabajar en las minas y otros empleos del barrio. Estaba situada en el muelle de Marzana donde hoy día está la escalera que conecta el muelle con la calle Bilbao La Vieja. Fue derribada en 1974."_ 
Fernan (14/11/2018).

![](img/04-casa-de-goma/diapo_13-casa-de-goma-pagina001.png)

# Plaza de Bilbao La Vieja

![](img/08-plaza-bilbi/diapo_27-plaza-bilbao-la-vieja-pagina001.png)

![](img/08-plaza-bilbi/diapo_28-plaza-bilbao-la-vieja-pagina001.png)

![](img/08-plaza-bilbi/diapo_29-plaza-bilbao-la-vieja-pagina001.png)

![](img/08-plaza-bilbi/diapo_30-plaza-bilbao-la-vieja-pagina001.png)

![](img/08-plaza-bilbi/diapo_31-plaza-bilbao-la-vieja-pagina001.png)






 

