[TOC]

# Origen 

Esta puebla ya existía cuando se creó la Villa en el año 1300, sobre los arenales de la margen izquierda del Ibaizábal. Puebla de pescadores que hacía muchos siglos posiblemente que la poblaban, se le llamó “Bilbao la Vieja” para distinguirla de la nueva población. 

[Fuente](http://sanfranbilbizabala.eus/es/barrios/)

Bilbao se fundó como tal en el año 1.300. Por aquél entonces, la urbe en la que hoy se ha convertido era un poblado que crecía poco a poco en torno al puerto y el Puente de San Antón.

La margen derecha de la ría, el Casco Viejo, era eminentemente comercial, y la izquierda, lo que hoy conocemos como **Bilbao La Vieja, minera. “Allende la Puente”** (nombre con el que se ha conocido aBilbao la Vieja hasta el siglo XVIII) vivía de la extracción del hierro de las minas de la zona de Miribilla. La abundancia de este mineral y su ubicación estratégica junto a la ría favoreció el comercio directo con el exterior y en poco tiempo, Bilbao experimentó un crecimiento espectacular.

# Finales del XIX, principios del sigo XX

Ya en el siglo XIX, con la Revolución Industrial, la **llegada masiva de trabajadores** de otras provincias e incluso países tuvo como consecuencia la expansión de Bilbao la Vieja hacia San Francisco.

**Los arrabales comprendían los barrios de San Nicolás, Achuri y Bilbao la Vieja**. Los suburbios obreros aglutinaban a los de San Francisco, Cortes y Zamácola.

En este nuevo ensanche se instalaron **familias de trabajadores, comerciantes y una pequeña burguesía**. San Francisco pasó de ser el terreno de un convento franciscano bajomedieval (de ahí su nombre) a una de las zonas más permisivas de Bilbao, centro de diversiones de todo tipo y famosa por su agitada vida nocturna.



# La prostitución (sigo XIX - XX)

[Fuente](http://www.euskonews.com/0582zbk/gaia58202es.html)

_"Bilbao disponía de 20 casas de prostitución legales entre 1875 y 1877, según la relación del Negociado de Higiene Pública9. Entre todas daban cobijo a 276 prostitutas sujetas a revisión médica y administrativa."_

_"La concentración de casas de prostitución de diversa clase y categoría en una zona muy concreta facilitaba el tráfico, al tener los clientes dos focos de expansión muy cercanos en la margen izquierda, el de **Urazurrutia-Cantarranas-Bilbao la Vieja, y el de Concepción, San Francisco-Mirivilla-La Fuente-Cortes**. Si a esto añadimos la alta densidad por habitante de las zonas, la extracción social de los barrios, la infravivienda, la falta de higiene, la proliferación de bares, cafés, tabernas, donde se expendía alcohol legal e ilegalmente, tendremos el caldo de cultivo necesario para convertirse en la zona con mayor índice de criminalidad de la villa."_

"_En el gráfico adjunto se puede apreciar la **distribución de casas de citas y prostíbulos declarados como legales en 1886**. Salvo la de la Encarnación en la margen derecha, el resto se encuentra en la margen izquierda. **La mayor concentración** ascendente de cruces rojas corresponde a la **actual zona de Mirivilla**, enclavada estratégicamente entre el cuartel militar y las minas, principales clientes de los burdeles."_

_"Pero, además de las mancebías registradas oficialmente en los barrios de las Cortes y San Francisco, existían otras zonas donde también se practicaba el comercio carnal solapado, como por ejemplo en la Gran Vía (en 1877 había tres locales con 20 internas) o el Casco Viejo (Ronda, Ascao)."_

![](img/prostitucion_casas_de_citas.jpg)

_"Las Cortes era el más degradado de los barrios obreros de Bilbao, con rasgos parecidos a los que ofrecían los núcleos mineros. Había nacido junto a las excavaciones de las minas y estaba poblado de inmigrantes llegados para trabajar en ellas, siendo además, el lugar donde radicaba la prostitución y las lacras sociales de Bilbao."_ García Merino, J.L, p 616.



# Imágenes

Casco Viejo y Bilbao la Vieja con el Puente de San Antón. Grabado del siglo XV.

![](img/Grabado-siglo-XV.jpg)

Bilbao en la segunda mitad del Siglo XVIII

![](img/bilbao-segunda-mitad-siglo-XVIII.jpg)

# Enlaces externos

* sanfranbilbizabala.eus ["Barrios"](http://sanfranbilbizabala.eus/es/barrios/).
* Bilbaopedia [Bilbao La Vieja. Calle](http://www.bilbaopedia.info/bilbao-vieja-calle).
* Wikipedia [Bilbao La Vieja](https://es.wikipedia.org/wiki/Bilbao_La_Vieja).