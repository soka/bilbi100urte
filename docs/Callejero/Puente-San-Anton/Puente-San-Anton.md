[TOC]

# Historia 

**Algunos creen que, este puente existe antes que la fundación de la Villa en el año 1300**. Fue durante siglos el único puente que cruzaba la ría. Los bilbaínos emplearon todos los recursos legales, y hasta violentos, para impedir que otro puente les arrebatara el monopolio en las comunicaciones entre las dos orillas. **Fue el emblema de Bilbao y este puente viejo, llamado, también, la puente vieja está reflejado en su escudo**. Es de origen medieval. Este puente tuvo una importancia histórica, ya que era paso obligado para el comercio de Vizcaya con Castilla, tras el privilegio otorgado en tiempos de doña María Díaz de Haro. **En el siglo XV, bajo este puente se empozaba a los delincuentes. El empozamiento era un método de ejecución muy arraigado que consistía en atar una piedra al cuello del condenado y arrojarla al agua. _"Aguaduchus"_​ y riadas lo derribaron en reiteradas ocasiones (existe constancia de su reconstrucción en madera en 1334 y de su posterior reconstrucción en piedra en 1463).

Para poner fin al problema, **el Ayuntamiento decidió sustituirlo a finales del siglo XIX, hacia 1870**, por uno nuevo, el actual, **situado, a diferencia del anterior, por delante de la iglesia de San Antón (aguas abajo)** y que empezó a funcionar hacia 1880 (El actual Puente de San Antón no está ubicado en el mismo sitio que el original). 

De hecho, ambos puentes convivieron desde la construcción del segundo, que finalizó en 1877, y la demolición del puente original en 1882.

Pablo de Alzola y Ernesto Hoffmeyer construyeron un segundo puente en 1877, pero fue destruido durante la Guerra Civil, en 1937. 

Después de sufrir las consecuencias de la última guerra carlista. Fue planificado por Hoffmeyer. **Fue reedificado, una vez más, en 1937, después de su voladura durante la Guerra Civil Española.**

# Imágenes

## Puente moderno

Bote atracado en la orilla.  

![](img/025.jpg)

![](img/13-biz-pag0809-4_11.jpg)

¿Cargadero de mineral? Hoy día se puede observar un saliente de la estructura aún.

![](img/San-Anton-14.jpg)

![](img/San-Anton-22.jpg)


## La gabarra 3 de Mayo de 1983

Un futuro que encarnaría la gabarra número 1, rebautizada en 1983 como Athletic Club

![](img/San-Anton-Gabarra.jpg)

![](img/San-Anton-23.jpg)

![](img/210.jpg)


![](img/48513431.jpg)

![](img/90965750.jpg)


## Barricada puente de San Anton 1906

Huelga minera? 

![](img/Barricada-puente-sananton-1906.jpg)


## Ring lucha grecorromana / libre ? 

Público observando desde el puente de San Anton.

![](img/Gabarra-Rin-1.jpg)

![](img/Gabarra-Rin.jpg)


![](img/San-Anton-1.jpg)

![](img/San-Anton-2.jpg)

![](img/San-Anton-3.jpg)

![](img/San-Anton-4.jpg)


![](img/San-Anton-10.jpg)

![](img/San-Anton-11.jpg)

![](img/San-Anton-12.jpg)

![](img/San-Anton-13.jpg)


![](img/San-Anton-15.jpg)

![](img/San-Anton-17.jpg)

![](img/San-Anton-18.jpg)

![](img/San-Anton-19.jpg)

![](img/San-Anton-20.jpg)

![](img/San-Anton-21.jpg)

![](img/San-Anton-24.jpg)

## Mercado San Anton 

![](img/San-Anton-25.jpg)

## Ría sucia 

![](img/San-Anton-26.jpg)

## Mercado de San Anton

![](img/San-Anton-27.jpg)

## Puente y iglesia

![](img/San-Anton-28.jpg)


## Dos puentes 

![](img/San-Anton-puente.jpg)

![](img/13-biz-pag0809-3_11.jpg)

![](img/images.jpeg)

## Pescadores y puente antiguo

![](img/s-l300.jpg)

## Puente antiguo 

![](img/San-Anton-16.jpg)

![](img/002.jpg)

![](img/13-biz-pag0809-2_11.jpg)

![](img/2012_12_10_04_12_27AL0011-0055.jpg)

Puentes original aguas abajo: 

![](img/38582461_PG3IShI9Ss0qdhKQ7lwTHIfyKeOK9_M0rL9bkAisJ50.jpg)

![](img/gaia32201_02.jpg)

![](img/arg0162.jpg)

![](img/San-Anton-8.jpg)

## 1880 

![](img/San-Anton-9.jpg)

## Primer puente de piedra 

![](img/C9ss2g3XkAA_XdA.jpg_large)

# Enlaces externos

* ["Vista de Bilbao con el puente viejo e Iglesia de San Antón"](http://www.euskonews.com/0162zbk/arg0162.html).
* Deia ["El puente de San Antón celebra el 140 cumpleaños de su construcción"](https://www.deia.eus/2017/10/13/bizkaia/bilbao/el-doble-cumpleanos-del-mas-viejo-de-bilbao).
* ["El puerto de Bilbao a través de la historia"](http://www.euskonews.com/0322zbk/gaia32201es.html).
* ["Bilbao, el origen de su nombre"](http://eukele.com/bilbao-el-origen-de-su-nombre/).
* ["5 curiosidades sobre el Puente de San Antón"](https://www.disfrutabizkaia.com/blog/5-curiosidades-sobre-el-puente-de-san-anton/).