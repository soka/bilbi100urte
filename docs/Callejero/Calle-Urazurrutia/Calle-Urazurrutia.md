[TOC]

Topónimo en euskera relacionado con una zona de agua abundante, debida a la Fuente de la Gabarra existente en este paraje.

# La Casa Cuna 

_"Hermoso edificio situado al inicio de la calle Urazurrutia, cerca del puente de San Antón. Fue **construido en 1912**, con una **decoración modernista de ladrillos, azulejos y cerámicas**, con el fin de **cuidar a los hijos de los trabajadores**. La casa cuna es un edificio obra **de Ricardo Bastida**."_

# Tranvía de Arratia

Foto del tranvía a su paso por la calle Urazurrutia, en el letrero con el destino de adivina que pone "CEANURI" con el número de vagón 15. Discurre junto a una fruteria. El "asfalto" está empedrado.

![](img/tranvia-calle-urazurrutia.JPG)

![](img/tranvia-muelle-urazurrutia.jpg)

Imagen: En 1954, el ayuntamiento de Bilbao obligó a **trasladar la terminal del tranvía a los muelles de Urazurrutia**. Fotografía de Juan Bautista Cabrera. Archivo EuskoTren/Museo Vasco del Ferrocarril.

Desde principios del siglo XX hasta la pasada la mitad de siglo discurre por la calle Urazurrutia un tranvía con destino a Durango y el valle de Arratia. 

**24 de febrero de 1954**: Restablecimiento total del servicio de tranvías entre Bilbao (Urazurrutia) y Zeanuri. [[Fuente]](http://www.arratia.net/es-ES/Noticias/Paginas/20151026_tranvia.aspx)

_"Las graves inundaciones que asolaron el País Vasco el 14 de octubre de 1953 dañaron en varios puntos la línea del tranvía de Arratia, al arrastrar el impetuoso flujo del Nervión el puente metálico situado en Urbi y destruir varios tramos del muro de ribera en los bilbainos muelles de Barandiaran y Urazurrutia, lo que supuso la inmediata paralización del tráfico tranviario entre Bilbao y Lemona..."

_" Mientras tanto, aprovechando la ausencia temporal de los tranvías por las calles de la villa, el Ayuntamiento de Bilbao decidió eliminar su acceso hasta el céntrico teatro Arriaga, **condenando al TBDA a finalizar su recorrido en los muelles de Urazurrutia**, en la orilla opuesta a la ocupada por la estación de ferrocarril de Atxuri. De este modo el tranvía perdía una de sus principales ventajas frente al tren; la comodidad que proporcionaba a sus viajeros poder llegar hasta el corazón de la ciudad."_


# Imágenes

## Año 2005

Donde hoy se puede ver una ancha artería de varios carriles de la calle Claudio Gallastegui subiendo al nuevo barrio de Miribilla antes existia un edificio de viviendas. 

![](img/01.jpg)

![](img/02.jpg)

![](img/03.jpg)

![](img/04.jpg)

![](img/05.jpg)

![](img/06.jpg)

![](img/07.jpg)

## 1953 Calle Urazurrutia

Casa Cuna a la izquierda. A aprecia alguna especie de raíl en la calzada. Al fondo la casa de Socorro y lo que pudiera ser el lavadero público delante. 

![](img/A-10-Boletin-Estadistico-de-la-Villa.1953-3er-trim.jpg)

## Rastro tras la casa cuna de Urazurrutia 

![](img/rastro-tras-casa-cuna-urazurrutia.jpg)

![](img/rastro-tras-casa-cuna-urazurrutia-02.jpeg)

# Enlaces externos

* [PDF]["La Casa Cuna en la calle Urazurrutia - Ayuntamiento de Bilbao"](http://www.bilbao.eus/bld/bitstream/handle/123456789/19557/pag08.pdf?sequence=1).
* ["Bilbaopedia - Escuelas de Urazurrutia"](http://www.bilbaopedia.info/escuelas-urazurrutia).
* ["Edificios singulares"](http://sanfranbilbizabala.eus/es/edificios-singulares/).
* bilbaoturismo.net ["LOS ALEDAÑOS DEL CASCO VIEJO"](http://www.bilbaoturismo.net/BilbaoTurismo/es/paseos-arquitectonicos/desde-atxuri-al-arenal).
* ["EL TRANVIA DE ARRATIA LLEGA A BILBAO"](http://www.arratia.net/es-ES/Noticias/Paginas/20151026_tranvia.aspx).
* ["El bombardeo fascista de los italianos contra Durango del 31 de marzo de 1937 acabó con el servicio de tranvía en la villa"](http://mugalari.info/2016/07/04/la-guerra-civil-acabo-la-compania-del-tranvia-bilbao-durango-arratia/).
* ["El tranvía de Arratia. Por Juanjo Olaizola Elordi"](http://www.ferropedia.es/wiki/El_tranv%C3%ADa_de_Arratia._Por_Juanjo_Olaizola_Elordi).
* ["EL TRANVÍA DE BILBAO A DURANGO Y ARRATIA (VII)"](https://historiastren.blogspot.com/2014/12/el-tranvia-de-bilbao-durango-y-arratia_15.html).
* ["EL TRANVÍA DE BILBAO A DURANGO Y ARRATIA (IIII)"](https://historiastren.blogspot.com/2014/12/el-tranvia-de-bilbao-durango-y-arratia_8.html).