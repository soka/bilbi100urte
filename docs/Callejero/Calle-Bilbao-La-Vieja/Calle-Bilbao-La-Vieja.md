[TOC]

* Situación. Empieza en Urazurrutia. Termina en Martzana.



# La transformación de la plaza de Bilbao La Vieja

En el año 2008 Surbisa realiza la remodelación de la [**Calle Bilbao La Vieja**](https://goo.gl/maps/NgVEYVqWo862) y de la [Plaza de los Tres Pilares](https://goo.gl/maps/MJ5BfAx8NFF2). El solar que conecta la plaza con la parte baja de Miribilla desaparece y es reemplazado por un edificio de viviendas de nueva construcción. 

# Imágenes

![](img/03.jpg)

![](img/01.jpg)

![](img/04.jpg)

![](img/07.jpg)

![](img/08.jpg)

![](img/09.jpg)


# Enlaces externos

* Bilbao SURBISA ["La transformación de la Plaza Bilbao la Vieja"](http://www.bilbao.eus/blogs/surbisa/2014/09/10/la-transformacion-de-la-plaza-bilbao-la-vieja/). 
* ["REHABILITACIÓN DE BARRIOS DESFAVORECIDOS Y PARTICIPACIÓN CIUDADANA - La experiencia en el Área de Bilbao la Vieja"](http://www.izangai.org/files/rehabilitacion-de-barrios-desfavorecidos-y-participacion-ciudadana.pdf) - Carlos Askunze. 