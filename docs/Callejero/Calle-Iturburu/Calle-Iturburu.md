[TOC]

La [**Calle Iturburu**](https://goo.gl/maps/vxzQmXJuiEK2) cuya traducción del euskera significa "Manantial", refiriéndose a la fuente de Urazurrutia.

# La casa de Iturburu nº 6

Nos permite ver un Bilbao que existio y que ya hoy no existe, edificaciones bajas de madera. 

_"El edificio que ocupaba el nº 6 de esta calle estaba considerada la casa más antigua de Bilbao, databa del siglo XV y era la única vivienda urbana que subsistía de esa época, hasta el 25 de enero del 2006, cuando fue demolida. Tenía planta baja y dos plantas en voladizo y un tejado a dos aguas. Era conocida como la casa de los pescadores porque en el frontón que estaba en la parte trasera jugaban a la pelota los marineros. Se ha conservado el arco gótico de piedra de sillería de la entrada para instalarlo en un parque público."_

_"La trama urbana de esta calle es el resultado de dos tipos de poblamiento superpuestos. El más antiguo es de origen medieval y está vinculado a su carácter de arrabal de la villa de Bilbao. Sobre él, y como consecuencia del crecimiento demográfico producido a finales del siglo XIX, se superpuso una nueva configuración en forma de suburbio obrero.

En Iturburu, 6 se alzaba hasta hace poco una de las casas más antiguas de Bilbao. Ocupaba un solar “gótico”, es decir, una parcela estrecha y profunda. Se databa su construcción en torno al siglo XV. Un elemento muy característico que permitía atribuirle esa cronología era su acceso en arco apuntado. Este elemento ha sido conservado tras la demolición de la casa y se reconstruirá en una plazoleta próxima."_

# Imágenes

![](img/portal-n-6.jpg)

![](img/05.jpg)

![](img/01.jpg)

![](img/02.jpg)

![](img/03.jpg)

![](img/07.png)

![](img/04.jpg)

# Enlaces externos

* 20minutos.es ["La casa más vieja de Bilbao es una ruina"](https://www.20minutos.es/noticia/85197/0/vieja/Bilbao/ruina/).
* bilbaopedia.info ["Iturburu. Calle"](http://www.bilbaopedia.info/iturburu-calle) - Javier González Oliver - [ 04.10.1933 ].
* Naiz.eus ["Vecinos de Iturburu piden al Consistorio bilbaino que acometa mejoras en su calle"](https://www.naiz.eus/eu/hemeroteca/gara/editions/2015-06-07/hemeroteca_articles/vecinos-de-iturburu-piden-al-consistorio-bilbaino-que-acometa-mejoras-en-su-calle) - AGUSTÍN GOIKOETXEA.
* bilbao.eus ["LA CALLE ITURBURU"](https://www.bilbao.eus/cs/Satellite/bilbaoLaVieja/LA-CALLE-ITURBURU/es/100257256/Contenido).
* ["Bilbao: Arte en la calle"](https://bilbaoarteenlacalle.wordpress.com/2018/02/28/arco-de-silleria/): Fotos del emplazamiento actual del "Arco de Sillería" de estilo gotico del edificio de la Calle Iturburu nº6.
* ["Derribos de la identidad"](https://www.revistaad.es/arquitectura/galerias/derribos-de-la-identidad/7859/image/603121): Foto antigua vivienda en Iturburu en blanco y negro.
   