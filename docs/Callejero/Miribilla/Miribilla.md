[TOC]

_«Calle que nace en la plaza de los Tres Pilares de Bilbao la Vieja y sube hasta la calle Concepción en la ladera del monte Miravilla del que toma su nombre»_

**Se llamaba en algún tiempo Miravilla** y empezaba en la plaza de los Tres Pilares y terminaba en La Concepción. Por decreto de Alcaldía del 26 de diciembre del 2000, se modificó y, desde ese momento, comienza en la plaza de los tres Pilares y termina en Arechaga.

**Su nombre, vocablo moderno, procede de su privilegiada situación**, al estar en un alto desde el que se divisa perfectamente la villa. Aunque en el siglo XX se difundió la forma Miravilla, sin alcanzar oficialidad afortunadamente, creada por etimología popular castellana, en opinión de lingüistas de prestigio como Mitxelena, se trata del topónimo, Biribila (redondo) con permutación de b inicial en m- un fenómeno común en euskera. Una de las etimologías más asentadas sobre el nombre de la villa lo relaciona con este topónimo al ser Bilbao una reducción de Biribilao, “la boca de Biribil”. Corresponde al cerro y terreno de veneras sobre el arrabal de **Allende la Puente**. Fue en otros tiempos, paseo obligado de los bilbaínos del siglo XV y XVI. El mirador de la villa, altozano para deleitarse a manera de las colinas romanas, aparece en el "Teatrum Orbis terrarum", de Abraham Ortellio, y las páginas dedicadas a trazar el panorama de Bilbao, en donde se ve la villa con su iglesia de San Antón, las Siete Calles con sus torres, la ría y las florestas que rodean la población. En primer término se advierten unas ma­tronas bilbaínas con diversos tocados y vestidos, que se dedicaban a pasear y contemplar el paisaje desde el alto o paseo de Miribilla.

Estos parajes, a los que llamaban calle de los Ferrones, se le denominó en el siglo XIX calle de don Diego López de Haro. Se le cambió el nombre por el de calle de la Fuente cuando se anexionó a Bilbao, ya que el nombre del fundador de la villa debía imponerse a una calle de más importancia.

En la sentencia dada en grado de revista de la pronunciada por el licenciado Cueto en el año de 1500, se lee: Corriendo el amojonamiento del camino de Ibaizábal y la subida a las veneras: «e de la dicha venera subiendo arriba derecho quedando a la parte de la villa el miradero para la mar que se llama las tres Arteas».

Sobre el año 1833, ante la proximidad de la que sería denominada Primera Guerra Carlista, sobre la cúspide del monte de Miribilla se reconstruyó el antiguo castillo, haciéndose un fuerte o fortín.

# Pasado minero

Gran parte del atractivo de Miribilla se encuentra bajo la superficie, ya que el subsuelo se encuentra completamente horadado por las minas de hierro activas antaño. El mineral era trasladado desde las minas a la ribera entre el puente de la Merced y el muelle de Urazurrutia, desde donde era remolcado por la ría hacia Olabeaga.

Una de esas minas era la de **San Luis**, recordada con la calle que rodea el parque de Miribilla y que **debe su nombre a su primer titular, un comerciante inglés llamado Luis Lewison**. Fue **clausurada en 1995** tras ocho años en los que permaneció inactiva, custodiada por Emilio Valdizán -minero encargado de cerrar por última vez la puerta de entrada a la galería- y una cuadrilla de operarios que veló por la seguridad de la caverna desde 1987. En la actualidad **se conserva intacto el túnel desde el que se evacuaba el mineral desde esta mina hasta la ría**. La puerta de entrada se encuentra en el número 2 del muelle de Marzana. Esta galería, de 90 metros en su parte más corta y 140 en la más larga, conserva las vías por las que circulaban las vagonetas.


# Barrio actual

El barrio de Miribilla se encuentra sobre un antiguo yacimiento de hierro, en cuya extracción trabajaban las ya desaparecidas minas de Abandonada, Malaespera y San Luis. En 1998 comenzaron las primeras obras de urbanización, iniciando la construcción de viviendas en 2000, cuya finalización comenzó a partir de 2003.


# Imágenes

![](img/minas_01.jpg)
-
![](img/minas_02.jpg)

![](img/minas_03.jpg)

![](img/minas_04.jpg)

![](img/minas_05.jpg)

![](img/minas_06.jpg)

![](img/minas_07.jpg)

![](img/minas_08.jpg)

![](img/minas_09.jpg)

![](img/minas_10.jpg)

Tranvía aéreo de transporte de vagonetas entre las minas de El Morro (Miraflores) y Miravilla, sobre las vías de la estación de Atxuri. 

_"De los yacimientos de Bilbao y sus inmediaciones se extrajo un ingente volumen de hierro. Esta circunstancia resulta fundamental para comprender el Bilbao de fines del XIX. Se transformó en un pueblo minero, al mismo tiempo que se convertía en la ciudad de los nuevos negocios industriales, financieros o navieros. En su entorno se levantaban barrios para los mineros."_

[(Fuente)](https://odiseaazul.blogspot.com/2016/06/tranvia-aereo-de-transporte-de.html) 

![](img/minas_11.jpg)


[(Fuente)](http://www.bioiron.info/eng/paseo_01.php?seccion=01_02)

![](img/minas_12.jpg)

# Enlaces externos

* El País["Un nuevo barrio entre minas"](https://elpais.com/diario/2000/02/28/paisvasco/951770420_850215.html).
* Bilbaopedia ["Miribilla. Calle"](http://www.bilbaopedia.info/miribilla-calle).
* Wikipedia ["Miribilla"](https://es.wikipedia.org/wiki/Miribilla).
* [PDF][«Ayuntamiento de Bilbao: Plano de la Villa.»](http://www.bilbao.net/euskera/ciudad/plano_callejero.pdf).
* [«"Urbanización del monte Miribilla" (Ricardo Ibarra Larrauri)».](https://sites.google.com/site/bilbaometropoli/proyectos/proyecto-14)
* El Correo ["El último minero de Miribilla"](https://www.elcorreo.com/vizcaya/prensa/20070422/vizcaya/ultimo-minero-miribilla_20070422.html).
* [PDF][«"Memoria minera en Miribilla"».](http://www.bilbao.eus/castella/residentes/vivebilbao/publicaciones/periodicobilbao/200703/pag02.pdf)
* El Correo [Miravilla](https://www.elcorreo.com/vizcaya/20070728/vizcaya/miravilla-20070728.html).
* ["Tranvía aéreo de transporte de vagonetas entre las minas de El Morro (Miraflores) y Miravilla, sobre las vías de la estación de Atxuri"](https://odiseaazul.blogspot.com/2016/06/tranvia-aereo-de-transporte-de.html).
* ["BILBAO DE ANTES, BLOQUE 4"](https://fotos-arquitectura.blogspot.com/2011/01/bilbao-de-antes-bloque-4.html).