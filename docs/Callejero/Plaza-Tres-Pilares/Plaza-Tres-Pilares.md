[TOC]

Casa de finales del siglo XIX, de tal peculiaridad constructiva que suplantó el anterior nombre, **"Plazuela de la Verónica"**.


Aunque tenga la denominación de “plaza”, no se trata de un espacio público planificado urbanísticamente sino de la confluencia de varias calles. Resulta bastante curioso el topónimo de “Tres Pilares”. Diversos estudiosos han atribuido su origen a alguna de las construcciones singulares que existirían en esta zona hasta finales del siglo XIX. En este sentido, apuntan a **la existencia de una casa torre situada entre la calle de la Concepción, esquina a Miravilla y que podría ser la identificada por otros como la Torre de Bilbao La Vieja**.

Cuando bajamos a la plaza de Bilbao La Vieja **a media cuesta destaca un gran arco de medio punto. Era el paso a un antiguo lavadero existente en la parte zaguera de las casas.**

Al l comienzo de la Calle San Francisco se encuentra la casa más pequeña de Bilbao. “Esta casa tiene la esquina más pequeña de la ciudad. Es testigo de la historia de San Francisco. 

# Enlaces externos

* ["Bilbaopedia - Tres Pilares. Plaza"](http://www.bilbaopedia.info/tres-pilares-plaza).
* ["PLAZA DE LOS TRES PILARES"](https://www.bilbao.eus/cs/Satellite/bilbaoLaVieja/PLAZA-DE-LOS-TRES-PILARES/es/100257201/Contenido).