[TOC]

# Calle Cantarranas

El nombre de esta calle **proviene o se refiere a un paraje encharcado de marismas donde se oía el croar de las ranas**. La [Calle Cantarranas](https://goo.gl/maps/7cXRp4973c32) antiguamente discurría por la parte trasera de la plaza de Bilbao La Vieja hasta conectar con la calle Iturburu. Hoy sólo queda el vestigio del acceso por las escaleras en la parte derecha de dicha plaza de Bilbao La Vieja. Desde esta calle se podía acceder a las antiguas minas.

Ya en el siglo XIX esta calle y otras cercanas cobijaban casas de prostitución, legales en aquella epoca.

_"Bilbao disponía de 20 casas de prostitución legales entre 1875 y 1877, según la relación del Negociado de Higiene Pública9. Entre todas daban cobijo a 276 prostitutas sujetas a revisión médica y administrativa."_

Durante la rehabilitación de Miribilla y Bilbao La Vieja algunos de sus edificios son demolidos y muchos de sus vecinos realojados en las nuevas viviendas que se construyen en el mismo lugar en el año 2006.
	
# Imágenes 

## El puentecito 1911

Presumimos que las siguientes fotos son de los años 90. Tal vez esta sea una de las fotos más antigua, muestra un puente desconocido hoy día. Se puede observar que subsiste el pilar del "puentecito", el cual parece que partía de la entrada de una vivienda hasta el otro lado de la calle. Es bastante peculiar.	

## Antiguas 

Fin de la calle Cantarranas y llega a la calle Ituburu (casa del siglo XV):

![](img/18.jpg)


## Años 80-90 en adelante 

![](img/17.jpg)

![](img/01.jpg)

![](img/02.jpg)

Las escaleras que suben paralelo a la muralla puede que acabasen en la calle Miribilla.

![](img/03.jpg)

![](img/04.jpg)

![](img/05.jpg)

![](img/06.jpg)

![](img/07.jpg)

![](img/08.jpg)

![](img/09.jpg)

![](img/10.jpg)

![](img/11.jpg)

![](img/12.jpg)

![](img/13.jpg)

![](img/14.jpg)

![](img/15.jpg)

![](img/16.jpg)

Demolición y construcción de Miribilla "moderna" al fondo:

![](img/19.jpg)




# Enlaces externos 

* ["BILBAO REENCONTRADO: BILBO80"](https://asislazcano.blogspot.com/2016/12/bilbao-reencontrado-bilbo80.html): Blog de Asís Lazcano. _"Yñarrón en una extraña  plaza (probablemente desaparecida o reformada) que había por Iturburu, Cantarranas..., detrás de Bilbao La Vieja. Vestigios de un Bilbao prenapoleónico."_.
* Wikipedia ["Bilbao La Vieja"](https://es.wikipedia.org/wiki/Bilbao_La_Vieja).
* ["HISTORIA DEL BARRIO CHINO DE BILBAO-Primera Parte"](https://memoriasclubdeportivodebilbao.blogspot.com/2012/06/historia-del-barrio-chino-de-bilbao.html).
* ["El Ayuntamiento derriba 12 edificios para poder conectar Miribilla y el Casco Viejo"](https://www.elcorreo.com/vizcaya/pg060428/actualidad/vizcaya/200604/28/ECD_miribilla.html).
* ["Un nuevo barrio entre minas"](https://elpais.com/diario/2000/02/28/paisvasco/951770420_850215.html) - El Pais - ISABEL CAMACHO - 28 FEB 2000.
* ["Mapa de la delincuencia bilbaína a través de sus distritos (siglo XIX). La prostitución en el barrio de Las Cortes"](http://www.euskonews.com/0582zbk/gaia58202es.html) - Andoni VERGARA MARTÍNEZ.
* ["LAS MINAS DE MIRIBILLA"](https://www.bilbao.eus/cs/Satellite/bilbaoLaVieja/LAS-MINAS-DE-MIRIBILLA/es/100257090/Contenido).
* ["Personas inmigrantes en el barrio de San Francisco"](http://www.euskonews.com/0123zbk/gaia12305es.html) - Beatriz Díaz.
* ["Paseando por el Casco Viejo de Bilbao"](https://bttysenderismoconanthercas.blogspot.com/2018/07/paseando-por-el-casco-viejo-de-bilbao.html).