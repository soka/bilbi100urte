[TOC]

# La “Casa Cuna”

La **“Casa Cuna”** de San Antón es un edificio ubicado en la calle Urazurrutia, junto al muelle del mismo nombre, en el barrio de Bilbao La Vieja. **Se construyó en 1912** según un proyecto del arquitecto **Ricardo Bastida** que posteriormente, en 1939, reformó el edificio añadiendo una nueva planta, diferenciada del resto y con un ligero vuelo sobre las fachadas en todo su perímetro.

La Casa Cuna tenía por objeto cuidar durante el día a los niños pequeños, hijos de jornaleros.

Es una obra de estilo modernista, que tuvo una presencia muy puntual en Bilbao. En este caso se combina con elementos neomudéjares destacando la **utilización de materiales como el ladrillo, el azulejo y la piedra**. El acercamiento de Bastida al Modernismo estuvo motivado por su estancia como estudiante de arquitectura en Barcelona, y en Bilbao se refleja casi exclusivamente en construcciones de servicios: lavaderos, Alhóndiga, Centro de Desinfección (Centro Cívico de Zankoeta).

El tratamiento de la fachada con amplios huecos y su solución constructiva consiguen dotar de unidad al conjunto, rematado por la cornisa superior. Su condición de edificio exento permite una amplia visión del mismo, incluso desde la margen opuesta de la Ría.

# Imágenes

![](img/01.jpg)

# Enlaces externos

* ["Bilbaopedia - Casa Cuna de Urazurrutia](http://www.bilbaopedia.info/casa-cuna-urazurrutia). 
* [PDF]["Escuelas de Urazurrutia - Ayuntamiento de Bilbao"](http://www.bilbao.eus/bld/bitstream/handle/123456789/14105/pag24.pdf?sequence=1).