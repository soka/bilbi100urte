[TOC]

# Las Escuelas de Urazurrutia

![](img/Foto-Antiguas-Escuelas.png)

Las **Escuelas de Urazurrutia** estaban situadas en el [nº 32 de la calle Urazurrutia](https://goo.gl/maps/uXr1xCRTLxJ2), en el barrio de **Bilbao la Vieja**, distrito de **Ibaiondo**. Ya desaparecidas, su lugar lo ocupa el Centro de Arte Contemporáneo [Bilbao Arte](https://bilbaoarte.org/).

Las antiguas escuelas datan de 1899, año en el que dio comienzo la construcción de las mismas, **siendo entregadas al Ayuntamiento en marzo de 1905** (una de las primeras de carácter municipal construidas en Bilbao).

Estas escuelas nacieron de la necesidad de dar servicios a una de las áreas en crecimiento y con mayor demanda de viviendas y más degradada de la ciudad.

El centro educativo ocupó el solar de la vieja cárcel Galera de la Villa, un edificio en mal estado, abandonado e inhabitable, que estaba aquejado de humedad y malos olores. El arquitecto Enrique Epalza fue el primero al que se le solicitó que preparase un proyecto para la construcción de un grupo escolar en la casa Galera de Urazurrutia, si bien se le acabó encargando el proyecto de la Casa Galera de Solokoetxe.

[**Gregorio Ibarreche Ugarte**](http://www.bilbaopedia.info/gregorio-ibarreche-ugarte), arquitecto municipal, fue el encargado de llevar a cabo la construcción del nuevo grupo escolar de Urazurrutia, diseñando un **edificio de corte neoclásico**, siguiendo parámetros estilísticos de sus obras anteriores, como es el caso del Palacio Ibaigane (1900), actual sede del Athletic
Club.

Tras verse **afectado por las inundaciones de 1983**, el edificio fue rehabilitado por los arquitectos municipales Elías Mas y Blanca Brea en 1994, abriendo sus puertas oficialmente en el **año 1998 como Centro de Arte Contemporáneo [Bilbao Arte](https://bilbaoarte.org/)**.

La función educativa fue asumida por unas nuevas escuelas situadas en Urazurrutia, 3, la Escuela Infantil Privada BBK Urazurrutia, dedicada a educación Infantil para niños y niñas de 0 a 3 años, modelo D. Esta escuela infantil se ubica en la antigua Casa Cuna de San Antón, edificio obra de Ricardo Bastida y construida en 1914.

# Imágenes

# Enlaces externos 

* ["Bilbaopedia - Escuelas de Urazurrutia"](http://www.bilbaopedia.info/escuelas-urazurrutia).
* ["Gregorio Ibarreche Ugarte"](http://www.bilbaopedia.info/gregorio-ibarreche-ugarte).
* ["La Casa Galera"](http://www.bilbao.eus/bld/bitstream/handle/123456789/17299/pag10.pdf?sequence=1).