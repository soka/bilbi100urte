[TOC]

# Urazurrutiako sorospen-etxea

Alaitz Ajuriagerra (2017)

XX. mende hasieran du sorrera eraikin honek. San Frantzisko eta Bilbo Zaharra kaleetako biztanleriaren premiei erantzuteko asmoz, D. Felipe Carretero eta D. Claudio Cerezo buru zirela, auzokide talde batek Bilboko udalari sorospen-etxe bat eraikitzeko eskaera egin zioten 1900. urtean. Izan ere, auzo horietako meatzaritzak erakarrita hazkundeak nabarmen egin zuen gora. Udalbatzak ordea, atzera bota zuen proposamen hura; Atxuri auzoan zegoen Santos Juanes Ospitale Zibila behar izan haiei
erantzuna emateko nahikoa zela argudiatuta.

1908.urte amaieran Basurtoko Ospitalea inauguratu zenean eta honek Atxurikoa ordezkatu zuenean, berriro birplanteatu zen sorostetxea eraikitzea. Hainbat gorabeheren ondoren, aldi baterako irtenbide gisa,Ospitale Zibileko sorospen zerbitzua mantentzea erabaki zen, baita udal langileak ere. Behintzat, kokapen berria aukeratu arte. 

San Anton elizaren alboan eraikitzeko proiektua deuseztatu eta azkenean Urazurrutiako kaian egitea erabaki zen, lehengo harraska publikoaren alboan, hain zuzen. Bien bitartean, Atxuriko Ospitale zaharra saldu, hau utzi eta Martzana kaian zegoen ikastetxeko etxabea erabili zen zerbitzu horiek emateko.

![](img/plano_fachada.jpg)

**Ricardo Bastida** udal arkitektoaren proposamen-proiektua ontzat eman eta 1914.urteko urtarrilaren 17an egin zitzaion harrera eraikinari. Barne aldea horrela zegoen antolatuta: beheko solairuan sendaketa gela, medikuaren kontsultategia, biltegia eta egongela; bigarren solairuan langileentzako logelak eta hirugarren solairuan atezainarentzat etxebizitza

![](img/1918_presupuesto_obras.jpg)

Bertako langileriari zegokionez, mediku batek, erizain batek eta atezainak osatzen zuten lan taldea martxan jarri zen unean. Geroxeago, etxez etxeko kontsultak erantzuteko beste mediku bat gehitu zen talde honetara. Urteak pasa ahala eta lanaldia murrizten joan zen heinean, gero eta langile gehiago heldu ziren sorospen-etxera eta 1983. urtean 5 atezainek, 5 erizainek eta
10 medikuk osatzen zuten bertako pertsonala. 

![](img/plano_planta_baja.jpg)

Hiribilduko biztanleriari eskainitako zerbitzuen artean hurrengoa nabarmendu daiteke: gaixoentzat etxez etxeko laguntza medikoa. Doako zerbitzua zen pertsona txiroentzat eta benefizentzian erroldatuta zeudenentzat. Dirudun jendearen kasuan, hauek, ondasunen arabera 3 multzotan banatu eta bakoitzari prezio ezberdin bat kobratzen zitzaion. Horrez gain, sorostetxeko kontsultan larrialdietako kasuei, lehen mailako zaurien sendaketei eta traumatismo gertaerei ematen zitzaien erantzuna.
Horiek ere debalde salbuespenak salbuespen. Larrialdietako kasu batzuetan erditzeak eta abortuak ere praktikatzen zituzten.


1983.urtean, abuztuaren 26an gertatutako uholdearen ondorioz, sotoa eta beheko solairua larriki kaltetu ziren eta eraikinaren hondamena ekarri zuen. Hori horrela, gaixoei erantzuteko zerbitzurik eman ezin eta udaletxeko arkitektoek eraikina ez birgaitzeko erabakia hartu zuten. Aldi baterako neurri gisa, sorostetxea Conde Mirasol kaleko lokaletara pasatu zuten, egungo Bilboko Berreginen Museoaren aldamenean zegoen udal langileentzako klinikara, alegia. Urte horretatik aurrera, pixkanaka-pixkanaka giza baliabideak murrizten joan ziren zerbitzua emateari utzi zen arte. Azkenean, 1989.urtean behin betiko itxi zen sorospen-etxea, baina garai hartako Jose Maria Gorordo alkateak Auzo Elkarteari eraikina erabiltzeko eta kudeatzeko baimena eman eta auzoarentzat kultur etxea izendatu zuten. 

Gaur egun, eraikina udalarena izan arren, Bilbo Zaharreko Auzo Elkartearen esku jarraitzen du honen erabilerak. Duela urte batzutik hona, auzokide talde batek kudeaketa lanak hasi, auzolanaren bitartez eraikinaren barnealdea konpondu eta jarduera
soziokulturalak eskaintzen hasi ziren bilbotarrentzat eta hurbildutako ororentzat. Hala nola, perkusio eta txalaparta tailerrak,
boxeoa, capoeira, euskara eta informatika klaseak, elkarteendako eta taldeentzako batzartzeko gelak eskaintzen dituzte eta
liburutegia martxan jartzen ari dira. Eraikinaren kanpo aldearen egoera ordea, abandonaturik dago udaletxearen esku sartze
eskasaren ondorioz. Alde batetik, auzokideek gutxieneko konponketak eskatzen dizkiote arduradunei, eta bestetik, Ricardo Bastida arkitektoak hirian diseinatu zituen beste eraikin garrantzitsu batzuek bezala mantentzea eta zaintzea. Hau da, Azkuna Zentroa (Alhondiga), Solokoetxe, Santutxu eta Deustu auzoetako etxebizitza askori emandako garrantzia ematea.

Iturria: Juan Gondra, Bilbao aldizkaria, 138 zenbakia, 2000ko maiatza


# Imágenes

## Plano fachada

Destaca un tejado plano con unos "pinaculos" en cada esquina. Grandes ventanas en anchura y sobre todo en altura.

![](img/plano_fachada.jpg)

## Plano planta baja

Plano de la planta baja de la Casa de Socorro, la distribución nos permite entender como funcionaba el servicio, nada más entrar a mano izquierda un despacho, a la derecha la garita del portero, la descripción de la sala contigua no se lee bien. Frente a las escaleras un baño. Cuando pasamos las escaleras llegamos a la sala de curas, una sala para material y un dormitorio. En el momento de dibujar el plano a la izquierda debía existir un lavadero público.   

![](img/plano_planta_baja.jpg)

## Presupuesto (1918)

El tejado plano hacia la funciones de terraza hoy perdidas a pesar de ser practicable.

![](img/1918_presupuesto_obras.jpg)

