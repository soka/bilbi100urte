[TOC]

# Desarrollo urbano de Bilbao, 1960

![](img/31052232257_afd1bf58c6_o.jpg)

# La Villa de Bilbao en 1960

![](img/31052239157_dcfc15242a_o.jpg)

# Tras la guerra, la paz facilito el gran crecimiento, año 1874

![](img/31052239907_7268afa3aa_o.jpg)

# 1874

![](img/31052241177_b8d4189687_o.jpg)

#  Bilbao, 1764

![](img/31052233007_ed3460e1c4_o.jpg)

# 1676

![](img/31052233817_65be8b32ec_o.jpg)

# 1676

![](img/31052234777_ae93421731_o.jpg)

# 1445

![](img/31052235487_46e337769d_o.jpg)

# 1445

![](img/31052236687_cbb7cc696a_o.jpg)

# 1350

![](img/31052237437_93f676d30c_o.jpg)

# III A.C 

![](img/31052238147_6522885733_o.jpg)

# 1350

![](img/31052242107_0eb02638c1_o.jpg)

# 1350

![](img/31052243277_5c9e3dfeda_o.jpg)

# 1300 

![](img/31052243897_a760fbe313_o.jpg)

# 1300 

![](img/31052245257_282da8f935_o.jpg)

# Begoña S. I

![](img/31052246907_02d592fb55_o.jpg)

# Malmasín 

![](img/31052247837_3890cce662_o.jpg)

# Siglo I

![](img/45941605992_9c372b073d_o.jpg)




