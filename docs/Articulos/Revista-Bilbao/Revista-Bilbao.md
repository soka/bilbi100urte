# Un siglo de historia en Bilbao. Casa de Socorro de Urazurrutia

* Autor: Juan Gondra

![](img/casa-socorro-urazurrutia-juan-gondra.jpg) 

# La casa de Iturburu (2001)

* Autor: Elias Mas Serra

![](img/2001-La-Casa-De-Iturburu.jpg)

# "Por un nuevo Bilbao La Vieja" (2002)

![](img/2002-por-un-nuevo-bilbao-la-vieja-01.jpg)

![](img/2002-por-un-nuevo-bilbao-la-vieja-02.jpg)

# Urazurrutia en Allende del Puente

![](img/Urazurrutia-Allende-del-Puente.jpg)



