[TOC]

# Introducción 

Muchas de las calles y edificios singulares que vamos a conocer por desgracia hoy no existen, muchas de sus gentes,  protagonistas de esta visita tampoco.

El objetibo es compartir un espacio y un tiempo donde los recuerdos de la gente más mayor puedan interrumpir la visita y despertar su imaginación para recrear como fue este barrio hace muchos años y que así los más jovenes escuchemos.


# Recorrido

* Partiendo del Puente de San Antón
* Casa Cuna 
* Casa Socorro
* Antiguas escuelas
* Iturburu
* Cantarranas
* Tres pilares
* Plaza Bilbao La Vieja