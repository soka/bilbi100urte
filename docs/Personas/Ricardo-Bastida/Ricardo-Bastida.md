[TOC]

# Ricardo Bastida (1879-1953)

![](img/2014-12-14_19_01_12_ricardo-bastida.jpeg)

_"Fue en el año **1914 cuando termina el proyecto de la Casa Cuna** de San Antonio en Urazurrutia."_



# Enlaces externos

* Bilbaopedia ["Ricardo Bastida y Bilbao"](http://www.bilbaopedia.info/bastida).

