[TOC]

![](img/julian-alegria-placa-plaza.jpg)

# Julián Ruiz de Alegría (1888-1956)

![](img/julian-alegria-caricatura.PNG)

_"Nació **Julián Ruiz de Alegría Pérez en Bilbao la Vieja en el año 1888.Hasta su fallecimiento en el año 1956 vivió en Urazurrutia, en lafamosa casa con un portal ojival**, hoy lamentablemente desaparecida.Estudió en el Patronato de Iturribide, iniciándose después en elaprendizaje del oficio de tallista, estudios que hubo de abandonar para hacerse cargo del **negocio familiar de fabricación de gaseosas y limonadas**. Él mismo se encargaba del reparto y distribución del producto, convirtiéndose en una figura familiar de la Villa, atavia-do con su txapela y su blusa azul, puro en ristre, y guiando sucarrito tirado por un bien enseñado asno. Fino humorista, acostumbraba a decir que vivía del agua para poder beber vino.Hombre de inquietudes varias, practicó la pelota y el ciclismo,participó en las excursiones montañeras de la época y formó partede agrupaciones teatrales de aficionados, que terminarían porconducirle a la Masa Coral del Ensanche, entidad especialmentededicada al género de la zarzuela, de la que llegó a ser Presidente. A su amor por la música debemos la salvación y conservación delcancionero bilbaino, ya que fue él quien se preocupó por rescatardel olvido las canciones populares y las coplas cantadas por las comparsas carnavalescas del Bilbao finisecular. Reunidas y publicadas en libros comoCarnavalescas bilbainas y Humorada Chimberiana, fueron base para la creación de conocidos grupos musicales que luego popularizarían por el mundo las bilbainadas: LosBocheros, Los Chimbos, Los Chimberos, Los Cinco Bilbainos… En su condición de tipochirene y amante de la tradición bilbaina, Julián Alegría dedicó también parte de su tiempo a recopilar,glosar y aderezar con jocosas anécdotas, las historias y andanzas deun buen número de personajes y tipos de aquel Bilbao pintorescoque entonces comenzaba ya a ser sólo un recuerdo y que plasmóen su obra Semblanzas de tipos populares"_

_"Aun recuerdo cuando en compañía de mi aita solíamos ir a visitarle a **su casa de Urazurrutia**. Nos salía a recibir en **su señorial portal, el único de arco ojival de la villa**. ¿Podría ser la antigua torre de Otxoa? Desde luego algunos la consideraban con la casa más antigua del bocho, a la par con la de Seberetxe."_

_"<<Muchas veces Joaquín Tejada (el de 'Los Bocheros') y yo acompañábamos a Julián Alegría camino de su casa, hasta la hornacina que guarda «el Santutxu» de Santiago en Urazurrutia.>>"_

# Imágenes

![](img/revista-bilbao-bilbainadas.png)

![](img/revista-bilbao-julian-alegria.png)

# Enlaces externos

* Euskal Museoa ["Página 33 - PERTSONAIA HERRIKOIAK" - JULIAN ALEGRIA](https://www.euskal-museoa.eus/public_data/pdf/publicaciones/2012_tipos_populares_eu_es/files/assets/basic-html/page33.html)
* [CARNAVALES EN BILBAO. EUSKAL MUSEOA](https://www.dantzanet.net/sites/default/files/inaki_carnavalesbilbaoweb.pdf)[PDF].
* Revista Bilbao ["Historias de las Bilbainadas (y II)"](http://www.bilbao.eus/bld/bitstream/handle/123456789/31798/40.pdf?sequence=1)(PDF).
